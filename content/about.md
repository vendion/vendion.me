+++
title = "About vendion"
date = "2018-01-01"
menu = "main"
type = "basic"
+++

## Summary

I am a software developer and a FLOSS enthusiast. I know a mixture of interpreted
and compiled languages such as: Go, Perl, PHP, Lua. I also know a little C and
C++, as well as learning Dart. I use VIM and Tmux on various Unix based systems,
mainly [Arch][arch], [openSUSE][suse], and [Ubuntu][ubuntu] Linux distributions
as well as [FreeBSD][fbsd]. I have some experience with using the
[Google App Engine][app-engine] for deploying applications to a cloud based
infrastructure. I also have experience maintaining several Git privately hosted
Git repositories, and Virtual Machines using the Qemu/KVM/Bhyve hypervisors.

[arch]: "https://www.archlinux.org/"
[suse]: "http://www.opensuse.org/"
[ubuntu]: "http://www.ubuntu.com/"
[fbsd]: "http://www.freebsd.org/"
[app-engine]: "https://developers.google.com/cloud/"

## Experience

### Systems Architect at Utiliflex

Oct 2016 - Current

Responsible for the design and implementation of all software as well as
integrating with 3rd party devices and systems. Porting and rearchitecting a
monolithic PHP codebase to a more modern client-server model using a mix of PHP
and Go languages.

### Developer at Code Journeymen

Jul 2013 - Oct 2016

Web development with Drupal CMS and PHP. Configuring Solr Search and Varnish
caching technology.

### Developer at Iron Gaming

May 2013 - Jul 2013

Development of backend APIs utilizing a combination of GO, MySQL, and MySQL
Cluster databases, deploying on Rackspace cloud infrastructure. Developed a web
application deploying on Google App Engine PAAS.

## Skills

*   Experience with Various Linux distributions and other flavors of Unix
    Operating Systems
*   SQL (MySQL, MariaDB, SQLite, PostgreSQL)
*   Shell scripting (Bash, Zsh)
*   Go
*   Perl
*   PHP
*   Application/Web Development
*   Git SCM
*   Vim

## Affiliations:

*   06 / 2007 - 04 / 2010 [Association of Computing Machinery][acm] - Student Member
*   05 / 2011 & 08 / 2012 [Chattanooga Unix Gnu Android Linux Users Group][chugalug] - President
*   10 / 2013 - 01 / 2019 [Google Developer Group (GDG) Gigcity][gdg] - Lead Community Organizer
*   02 / 2014 - 12 / 2014 [Chattanooga Python User Group][chapython] - Lead Organizer
*   02 / 2014 - current [Chadev][chadev] - Organizer
*   01 / 2015 - current [Chattanooga Python User Group][chapython] - Co-organizer

[acm]: "http://www.acm.org/"
[chugalug]: "http://chugalug.org/"
[gdg]: http://www.gdggigcity.com/
[chapython]: "http://www.meetup.com/Chattanooga-Python-User-Group/"
[chadev]: "http://chadev.github.io/"

## Download Resume

There is also downloadable version of my Resume, suitable for printing or
offline viewing, or access it via my API.

[Download Resume](https://docs.google.com/document/d/1Rfy9DXqLzndySsNT9Mn5I4lSt9qiq1vlKaHr49cJ2lg?usp=sharing)
