+++
title = "trashctl"
date = "2018-01-02T20:34:19-05:00"
created = "2014-03-05T00:00:00-05:00"
source = "https://github.com/vendion/trashctl"
current = true
+++

Trashctl brings the concept of a a trash bin, also known as a recycle bin, to the
command line. The goal of trashctl is to be simple, work with not only local
storage but have basic support cloud syncing services. Trashctl should also work
with file managers, so that changes made via trashctl will be visible to them as
well.
