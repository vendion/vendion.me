+++
title = "Blog Rewrite"
date = "2013-09-03T11:27:11Z"
Categories = ["blog"]
Tags = ["golang", "perl"]
draft = false
+++

## Blog rewrite, this time in Go!

Some time after re-launching my website, which was a complete rewrite from Perl
to Go, and now the blog piece is getting the same treatment. I have been writing
in Go for three years now, and in that time there have been a lot of great
write ups about other services that are either being developed in or rewritten in
Go which to someone like me this is always good news.

Don’t get me wrong, Perl is a great language, but using it and the
[Catalyst Framework][catalyst] was too much overhead for
what was needed. In my use case I was having to run Apache as a proxy server to
redirect requests to a secondary web server, in this case it was [Starman][starman],
that was running the Perl application. After the rewrite I was able take all this
down to one service, lowering the load on my server while at the same time
speeding up response time.

While I wish that I had fancy benchmarks that can back up my claims or would be
of interest, I did not get benchmarks of the old perl version of the site. As
previously stated the version is built in Go, and with with rewrite I decided to
do some things differently. With this chance one thing I decided to change was
how posts are entered. Instead of entering posts in HTML, I have decided to
enter data in markdown then use [blackfriday][blackfriday] package to convert the markdown
to HTML when a blog post is viewed. This gives me a way to quickly and easily
write out a post, while at the same time outputting clean HTML that will be viewed.

[catalyst]: http://www.catalystframework.org/ "Catalyst Perl MVC Framework"
[starman]: https://metacpan.org/module/MIYAGAWA/Starman-0.4006/script/starman "Starman Launcher"
[blackfriday]: https://github.com/russross/blackfriday "Blackfriday Markdown parser"
