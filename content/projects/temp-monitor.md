+++
title = "Temp Monitor"
date = "2021-08-05T22:44:22-04:00"
created = "2019-11-14T01:20:42-00:00"
source = "https://sr.ht/~vendion/temp-monitor/"
current = true
+++

Temp Monitor is a simple in home temperature monitor, which pulls weather data
from OpenWeatherMap and reads from a BME280 sensor. This project is mainly an
experiment with working with WASM in Go as well as reading sensor data with an
Onion Omega 2+.
