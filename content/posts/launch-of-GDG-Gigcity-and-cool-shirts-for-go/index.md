+++
title = "Launch of GDG Gigcity and Cool Shirts for Go"
date = "2013-11-01T12:05:42Z"
Tags = ["golang", "GDG", "google", "usergroups"]
Categories = ["blog"]
draft = false
+++

I am pleased to announce the official launch of the Google Developer Group
[GDG Gigcity][gdggigcityg+], the group can also be found on
[meetup][gigcitymeetup]. This group is open to anyone in the Chattanooga
Tennessee area, our meetings will always be free for anyone to attend.
To help start things off and get the word out, there has also been a
[Go lang T-shirt][golangshirt] designed by [Charles Fannin][charlesfannin].
Even though the shirt was designed by Charles nether he or I make any money off
of any sales, it was just something cool done in his spare time, and I am promoting
because who doesn’t want their own custom shirts?

The goal of GDG Gigcity is to be a group for those interested in
[Google based technology][googledev] be it Android, App Engine, Chrome, Dart, and
even Go. If you use a Google based technology in your work or even on a pet
project or if you would like to know how you can make use of such things this
group is for you. This will be an open group where members can present on
things that they use or like.

If you would like to join the group, or just check it out, please visit either
the [Google Plus page][gdggigcityg+] or the [Meetup][gigcitymeetup] group where
you can get more information and join.

[gdggigcityg+]: https://plus.google.com/102911015778633923479/about "GDG Gigcity Google+ Page"
[gigcitymeetup]: http://www.meetup.com/GDG-Gigcity/ "GDG Gigcity Meetup Page"
[golangshirt]: http://www.customink.com/lab?cid=uzn0-000x-q0sa
[charlesfannin]: https://plus.google.com/b/102911015778633923479/106716840048014599736/about
[googledev]: https://developers.google.com/
