+++
title = "Personal Highlights From Drupalcamp Asheville 2014"
date = "2014-08-29T22:59:38Z"
Tags = ["Drupal", "Git", "Tech Talks"]
Categories = ["blog"]
draft = false
+++

Last weekend, August 22-23, I attended [Drupal Camp Asheville][dcashville] for
the first time. This was also the first time giving a talk at a Drupal camp,
after going to various camps in the past year and even attending [Drupalcon Austin][drupalcon]
I noticed that most, if not all, of the talks on version control, let alone on
Git, were focused on beginners. While I’m not saying this is a bad thing, people
should be giving these talks as version control systems is something people need
to know. So instead I decided to give a “not your average Git talk”, titled
[“How not to be a Git”][hntbag], focusing more on tip and tricks as well as
introducing people to some of the more powerful and useful features of Git.

While the [talk itself][gitslides] was done pretty much last minute, doing most
of it in the passenger seat of my bosses van on the way to Asheville, North
Carolina, everything worked out. My session was scheduled first thing in the
morning, and honestly I was not expecting the turn out that I got. By the end
of it I knew I just blew some minds, and there were some really good questions
at the end as well (sorry don’t remember what they were). I even had a few
people stay in the room after the talk for some in depth questions. Through out
the day I had several people tell me how much they enjoyed my session and that
they were able to take something away from the talk. What I was not expecting
from it though, was when my boss and the organizer of the camp pointed out that
my session was brought up multiple times in [a recent blog post about the
camp][mediacurrent] and that blew my own mind.

![Mind blown gif](http://i.imgur.com/Pmxoooa.gif)

For those that were unable to attend Drupal Camp Asheville, or unable to attend
my session there I will be doing a “new and improved” version at [Drupal Camp
Chattanooga](https://www.drupalcampchattanooga.com/ "Drupal Camp Chattanooga")
on September 13th.

[dcashville]: http://drupalasheville.com/camp/2014 "Drupal Camp Asheville 2014"
[drupalcon]: https://austin2014.drupal.org/node "Drupalcon Austin 2014"
[hntbag]: http://drupalasheville.com/drupal-camp-asheville-2014/sessions/how-not-be-git "How Not To Be A Git @ Drupal Camp Ashville 2014"
[gitslides]: https://docs.google.com/presentation/d/1BCKk4bkmoMeTqVZTROFQ8idHMJuaXnougSOdOTdR_c0/edit?usp=sharing "How Not To Be A Git slides"
[mediacurrent]: http://www.mediacurrent.com/blog/highlights-drupalcamp-asheville "Mediacurrent Highlights from Drupalcamp Ashville"
