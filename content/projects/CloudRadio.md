+++
title = "Cloud Radio"
date = "2018-01-02T20:33:43-05:00"
created = "2013-04-15T00:00:00-05:00"
source = "http://cloud-radio.appspot.com/"
current = true
+++

Cloud Radio is a web based HTML 5 (no flash), web radio player, utilizing local
storage in the browser to store the different radio stations thus no need for an
account!
