+++
title = "Gsnappy"
date = "2018-01-02T20:34:05-05:00"
created = "2012-02-01T00:00:00-05:00"
source = "https://github.com/vendion/gsnappy"
current = false
+++

Gsnappy is a archive tool that makes use of Snappy compression library developed
by Google.
