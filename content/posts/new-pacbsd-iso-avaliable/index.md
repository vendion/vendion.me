+++
title = "New Pacbsd Iso Avaliable"
date = "2016-07-21T00:27:33Z"
Tags = ["PacBSD"]
Categories = ["blog", "Press Announcement"]
draft = false
+++

The PacBSD team is happy to announce that a new installation image in finally
available for download for 64 bit x86 (x86_64) based computers. PacBSD is the
rebranded successor to ArchBSD along with a new name comes several changes as
well. For starters all of our packages have been rebuilt from the ground up with
the focus of matching the versions that FreeBSD’s Ports tree has rather than
trying to match what Arch Linux has which lead us into several issues due to the
underlaying GNU/Linux vs BSD differences and we lacked the developer base to be
able to resolve them. We also switched to using FreeBSD’s architecture names
rather than what Linux uses so instead of “x86\_64” we now have “amd64”.

Currently we are only supporting amd64 based machines, we may add i386 and
ARM/ARM64 support in the future. We have two different install media a ISO for
CD/DVD use and a IMG for USB devices. Be sure to select the right file for your
use case when downloading. These are available for download at
https://packages.pacbsd.org/iso. Currently the main packages available for
testing are: [LXDE](http://lxde.org/), [Chromium](https://www.chromium.org/),
[Xorg](https://www.x.org/wiki/), [wine](https://www.winehq.org/),
[transmission](https://www.transmissionbt.com/), and a few Window Managers.
New packages are added daily and more DE should be available in a few days.
[Xfce4](http://www.xfce.org/), [Firefox](https://www.mozilla.org/en-US/firefox/new/),
and [VLC](http://www.videolan.org/index.html) will be added to the repositories
next. Though there are multiple PKGBUILDs for these already available at
https://github.com/PacBSD/abs. You can view daily reports of the repository
statuses, which includes broken packages, packages which fail to pull in
dependencies, outdated packages and other information anytime at
http://users.pacbsd.org/~repo/repo-report/. Installation help can be found at:
https://wiki.pacbsd.org/Official_Arch_BSD_ZFS_Install_Guide. If you need
additional help, feel free to join #pacbsd-chat or #pacbsd-dev on Freenode. All
new uploaded packages, git commits, repository reports are posted to #pacbsd-dev
daily as well.

If you run into any issues while installing or running PacBSD, please report them
to https://bugs.pacbsd.org. For more information please see the the original
[release announcement](http://www.pacbsd.org/news/new-iso-available/). To learn
more about the project visit [our website](https://www.pacbsd.org/).
