+++
title = "ssh-manage"
date = "2018-01-02T20:34:12-05:00"
created = "2015-02-08T00:00:00-05:00"
source = "https://sr.ht/~vendion/ssh-manage/"
current = true
+++

ssh-manage is a utility designed to help with managing openSSH's configuration
file. It helps with managing, adding, updating, and removing host entries with
ease and with portability in mind. It is designed to work in BSD, Linux, and
MacOS environments where the location of the user's home directory may differ.
