+++
title = "Accessing My Personal Api"
date = "2015-02-08T23:26:51Z"
Tags = ["API", "Resume"]
Categories = ["blog"]
draft = false
+++

As a software developer I love working with APIs for different things, after
seeing someone else put together their own “personal API” it got me thinking why
don’t I have my résumé available via an API? The API should be as stateless as
possible, and only one endpoint available to a GET request. While its no where
near being completed, it is in a state where I feel that I can go ahead and make
it public. My personal API can be accessed either by a web browser or any other
web tool at https://vendion.me/api and returns JSON. There are countless reasons
for picking JSON over other formats for the return value that I won’t get into
here other than the fact that JSON is considered human readable when compared
to a format like XML.
